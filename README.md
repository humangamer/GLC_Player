# GLC_Player
A program to view and convert 3D models.
Originally from https://github.com/laumaya/GLC_Player and https://github.com/laumaya/GLC_lib

# Build Instructions
## Requirements
* Visual Studio 2017
* Qt 5.9.2 x64 (incl. Visual Studio Plugin) **Other 5.x.x versions may also work.**

## Instructions
1. Open GLC_Player.sln with Visual Studio 2017
2. Click on Build in the menu bar and select Build Solution.

The project should now build the selected configuration (ie. Debug_x64 or Release_x64).
The build files can be found in the Debug_x64 or Release_x64 directory depending on your configuration.
